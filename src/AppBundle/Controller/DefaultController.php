<?php

namespace AppBundle\Controller;

use Doctrine\DBAL\Connection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /** @var Connection $conn */
        $conn = $this->getDoctrine()->getConnection();
        $sql = "SELECT COUNT(*) as total, created_by, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at FROM personnels GROUP BY created_by, DATE_FORMAT(created_at, '%Y-%m-%d')";
        $result = $conn->fetchAll($sql);
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'statistics' => $this->prepareReportData($result),
            'users' => $users
        ]);
    }

    public function prepareReportData($result)
    {
        $data = [];
        foreach ($result as $row) {
            $data[$row['created_at']][$row['created_by']] = $row['total'];
        }

        ksort($data);

        return $data;
    }
}
