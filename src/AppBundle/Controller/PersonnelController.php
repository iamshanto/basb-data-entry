<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\PersonnelDatatable;
use AppBundle\Entity\Personnel;
use AppBundle\Form\PersonnelType;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Personnel controller.
 *
 * @Route("personnel")
 */
class PersonnelController extends Controller
{
    /**
     * Lists all personnel entities.
     *
     * @Route("/", name="personnel_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        // Get your Datatable ...
        //$datatable = $this->get('app.datatable.post');
        //$datatable->buildDatatable();

        // or use the DatatableFactory
        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(PersonnelDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
            $qb = $datatableQueryBuilder->getQb();
            $qb->addOrderBy('personnel.updatedAt', 'DESC');

            if (!$this->isGranted('ROLE_ADMIN') && !$this->isGranted('ROLE_QC')) {

                $username = $this->getUser()->getUsername();
                $qb->andWhere($qb->expr()->eq('personnel.createdBy', ':username'));
                $qb->setParameter('username', $username);
            }

            return $responseService->getResponse();
        }

        return $this->render(
            'personnel/index.html.twig',
            array(
                'datatable' => $datatable,
            )
        );
    }

    /**
     * Creates a new personnel entity.
     *
     * @Route("/new", name="personnel_new", options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $personnel = new Personnel();
        $form = $this->createForm('AppBundle\Form\PersonnelType', $personnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $personnel->setCreatedAt(new \DateTime());
            $personnel->setUpdatedAt(new \DateTime());
            $personnel->setCreatedBy($this->getUser()->getUsername());
            $personnel->setUpdateBy($this->getUser()->getUsername());
            $em->persist($personnel);
            $em->flush();

            return $this->redirectToRoute('personnel_index');
        }

        return $this->render(
            'personnel/new.html.twig',
            array(
                'personnel'        => $personnel,
                'form'             => $form->createView(),
                'autoCompleteData' => $this->getAutocompleteData()
            )
        );
    }

    /**
     * Finds and displays a personnel entity.
     *
     * @Route("/{id}", name="personnel_show")
     * @Method("GET")
     */
    public function showAction(Personnel $personnel)
    {
        $deleteForm = $this->createDeleteForm($personnel);

        return $this->render(
            'personnel/show.html.twig',
            array(
                'personnel'   => $personnel,
                'delete_form' => $deleteForm->createView()
            )
        );
    }

    /**
     * Displays a form to edit an existing personnel entity.
     *
     * @Route("/{id}/edit", name="personnel_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Personnel $personnel)
    {
        $deleteForm = $this->createDeleteForm($personnel);
        $editForm = $this->createForm('AppBundle\Form\PersonnelType', $personnel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $personnel->setUpdatedAt(new \DateTime());
            $personnel->setUpdateBy($this->getUser()->getUsername());

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('personnel_index');
        }

        return $this->render(
            'personnel/new.html.twig',
            array(
                'personnel'        => $personnel,
                'form'             => $editForm->createView(),
                'delete_form'      => $deleteForm->createView(),
                'autoCompleteData' => $this->getAutocompleteData()
            )
        );
    }

    public function getAutocompleteData()
    {
        return [
            'corp'             => $this->getCorp(),
            'rank'             => $this->getRank(),
            'trade'            => $this->getTrade(),
            'retirementReason' => $this->getRetirementReason(),
            'education'        => [
                'পঞ্চম শ্রেণী',
                'ষষ্ঠ শ্রেণী',
                'সপ্তম শ্রেণী',
                'অষ্টম শ্রেণী',
                'এসএসসি',
                'এইচএসসি',
                'অনার্স',
                'মাষ্টার্স'
            ],
            'district' => PersonnelType::getDistricts(),
            'upazilla' => PersonnelType::getUpazilla(),
            'children' => ['ছেলে', 'মেয়ে'],
            'inheritor' => ['বাবা', 'মা', 'স্ত্রী', 'ছেলে', 'মেয়ে'],
            'discipline' => ['দৃষ্টান্তমূলক', 'দৃষ্টান্তমুলক','আদর্শ','ভাল','সন্তোষজনক','অসন্তোষজনক'],
            'service' => ["এমওডিসি", "নৌবাহিনী", "প্রাক্তন ব্রিটিশ", "বিমান বাহিনী", "সেনা বাহিনী"]
        ];
    }

    /**
     * Deletes a personnel entity.
     *
     * @Route("/{id}", name="personnel_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Personnel $personnel)
    {
        $form = $this->createDeleteForm($personnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($personnel);
            $em->flush();
        }

        return $this->redirectToRoute('personnel_index');
    }

    /**
     * Creates a form to delete a personnel entity.
     *
     * @param Personnel $personnel The personnel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Personnel $personnel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personnel_delete', array('id' => $personnel->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function getTrade()
    {
        $data = [
            'জিডি',
            'ক্লার্ক',
            'এয়ার ক্রাফট ফিটার',
            'ইঞ্জিন ফিটার',
            'জেনারেল ইঞ্জিনিয়ার',
            'ইন্সটুম্যান ফিটার',
            'ইলেকট্রিক ফিটার',
            'আর্মামেন্ট ফিটার',
            'রাডার অপারেটর',
            'রাডার ফিটার',
            'ওর্য়ালেস ফিটার',
            'এলএসইডবি‘উ',
            'ফটো',
            'এনটিওএফ',
            'মিউজিক',
            'সেক্রেটারী এ্যাসিসটেন্ট (জিডি)',
            'সেক্রেটারী এ্যাসিসটেন্ট (হিসাব)',
            'এ্যাডমিন এ্যাসিসটেন্ট',
            'ক্যাটারিং এ্যাসিসটেন্ট',
            'জিএস',
            'এটিসি',
            'ম্যাট',
            'মেডিক্যাল এ্যাসিসটেন্ট',
            'প্রভোস্ট',
            'জিসিআই/পিটিআই',
            'জিসি',
            'জিডি',
            'ক্লার্ক',
            'সি ম্যান',
            'আরওজি/কমিনিউকেশন',
            'রেগুলেটিং',
            'মেডিক্যাল',
            'ইলেকট্রিক্যাল',
            'রেডিও ইলেকট্রিক্যাল',
            'ইঞ্জিনিয়ার',
            'রাইটার',
            'ষ্টোর',
            'কুক',
            'ষ্টুয়ার্ড',
            'স্টুপাস',
            'ক্লার্ক',
            'জিডি',
            'পাঁচক ইউ',
            'পাঁচক মেস',
            'টেইলার',
            'কার্পেন্টার',
            'এমটি',
            'ইএন্ডবি আর',
            'ড্রাইভার  এ আর ভি',
            'ডিএমটি',
            'ফিডার',
            'ওসিইউ',
            'ওপিআর',
            'টিএ',
            'ফিটার',
            'গানার',
            'ইএন্ডবিআর',
            'এই',
            'ওইপি',
            'ডিএসভি',
            'মেশন',
            'সার্ভেয়ার',
            'ড্রাফটসম্যান',
            'মেরিন ড্রাইভার',
            'পেইন্টার',
            'এসএমএস',
            'এসএমটি',
            'মেডিক্যাল এসিস্টেন্ট',
            'ওটি',
            'ল্যাব',
            'ফিজিও',
            'রেডিও',
            'পিএমএ',
            'এসটিএ',
            'আইসিএ',
            'স্টেনো',
            'টিএভি',
            'টিবিভি',
            'টিসিভি',
            'আর্মোরার',
            'ওয়েল্ডার'
        ];

        return array_unique($data);
    }

    public function getCorp()
    {
        return [
            'আর্মাড',
            'আর্টিলারি',
            'ইঞ্জিনিয়ার্স',
            'সিগন্যাল',
            'ই বেংগল',
            'বীর',
            'আর্মি এ্যাভিয়েশন',
            'এএসসি',
            'এএমসি',
            'অর্ডন্যান্স',
            'ইএমই',
            'আরভিএন্ডএফসি',
            'আর্মি ডেন্টাল কোর',
            'সিএমপি',
            'এইসি',
            'এসিসি',
            'পোষ্টাল',
            'এএফএনএস',
            'অপারেশন এবং প্রশাসনিক সদর দপ্তর',
            'অপারেশন ইউনিট',
            'সার্পোট বেজ',
            'প্রশিক্ষণ ইউনিট',
            'সংরক্ষিত বিভাগ',
            'বিমান সদর দপ্তর',
            'বিমান সদর দপ্তর (ইউনিট)',
            'এয়ার ডিফেন্স অপারেশন সেন্টার'
        ];
    }

    public function getRank()
    {
        return [
            'ফিল্ড র্মাশাল',
            'জেনারেল',
            'লেঃ জেনারেল',
            'মেজর জেনারেল',
            'ব্রিগেডিয়ার জেনারেল',
            'কর্ণেল',
            'লেঃ কর্ণেল',
            'মেজর',
            'ক্যাপ্টেন',
            'লেঃ',
            '২লেঃ',
            'মাষ্টার ওয়াঃ অফিঃ',
            'সিনিঃ ওয়াঃ অফিঃ',
            'ওয়াঃ অফিঃ',
            'সার্জেন্ট',
            'কর্পোঃ',
            'ল্যাঃ কর্পোঃ',
            'সৈনিক',
            'সিপাহী',
            'পাইওনিয়ার',
            'স্যাপার',
            'সিগন্যাল',
            'সোয়ার',
            'এসকিউএমএস',
            'এসএসএম',
            'জিসি জিডবি‘ঊ¢ঢ়',
            'জিডবি‘ঊ¢ঢ়',
            'এনসি(ই)',
            'এনসি(ইউ)',
            'রিক্রুট',
            'আরটি',
            'এ্যামিরল',
            'ভাইস এ্যাডমিরল',
            'রিয়ার এ্যাডমিরল',
            'কমডোর',
            'ক্যাপ্টেন',
            'কমান্ডার',
            'লেঃ কমান্ডার',
            'লেঃ',
            'সাব লেঃ',
            'এ্যাকটিং সাব লেঃ',
            'অফিসার ক্যাডেট',
            'এমসিপিও',
            'সিপিও',
            'পিও',
            'এলএস',
            'এবি',
            'ওডি',
            'র্মাশাল অব দ্যা এয়ার ফোর্স',
            'এয়ার চীপ র্মাশাল',
            'এয়ার র্মাশাল',
            'এয়ার ভাইস র্মাশাল',
            'এয়ার কমডোর',
            'গ্রুপ ক্যাপ্টেন',
            'উইং কমান্ডার',
            'স্কোয়াড্রন লীডার',
            'ফ্লাইট লেঃ',
            'ফ্লাইং অফিসার',
            'পাইলট অফিসার',
            'ফ্লাইট ক্যাডেট',
            'ক্যাডেট',
            'মাষ্টার ওয়াঃ অফিঃ',
            'সিনিঃ ওয়াঃ অফিঃ',
            'ওয়াঃ অফিঃ',
            'সার্জেন্ট',
            'কর্পোঃ',
            'এলএসি',
            'এসি-১',
            'এসি-২',
            'রিক্রুট',
            'সৈনিক',
            'ল্যান্স কর্পোরাল',
            'কর্পোরাল',
            'সার্জেন্ট',
            'ওয়াঃ অফিঃ',
            'সিনিঃ ওয়াঃ অফিঃ',
            'মাষ্টার ওয়াঃ অফিঃ'
        ];
    }

    public function getRetirementReason()
    {
        $result = $this->getDoctrine()->getConnection()->fetchAll("SELECT DISTINCT retirement_reason FROM personnels");
        $data = [];
        foreach ($result as $row) {
            $data[] = $row['retirement_reason'];
        }
        return array_merge($data, [
            'AR(1)-171(K)',
            'AR(I)-171(J)',
            'AR(I)-171(L)',
            'AR(I)-171(M)',
            'AR(I)-171(N)',
            'AWOL',
            'Court Martial',
            'Dependent Pension',
            'Dismissed from Svc',
            'EXPIRED',
            'Medical Ground',
            'Normal Retirement',
            'Own Request',
            'Pakistan Pensioner',
            'Premature Retirement',
            'Reservist',
            'STMK(ARI-171{J})'
        ]);
    }
}