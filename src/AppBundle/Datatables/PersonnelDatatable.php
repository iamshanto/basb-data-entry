<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class PersonnelDatatable
 *
 * @package AppBundle\Datatables
 */
class PersonnelDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'classes' => Style::BOOTSTRAP_3_STYLE . ' table-condensed', // default: Style::BASE_STYLE
        ));

        $this->features->set(array(
            'state_save' => true
        ));

        $this->columnBuilder
            ->add('personnelNumber', Column::class, array(
                'title' => 'Personnel Number',
                'orderable' => false
                ))
            ->add('designation', Column::class, array(
                'title' => 'Designation',
                'orderable' => false
                ))
            ->add('name', Column::class, array(
                'title' => 'Name',
                'orderable' => false
                ))
            ->add('regiment', Column::class, array(
                'title' => 'Regiment',
                'orderable' => false
                ))
            ->add('trade', Column::class, array(
                'title' => 'Trade',
                'orderable' => false
                ));

            if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                $this->columnBuilder->add('createdBy', Column::class, [
                    'title' => 'Created By'
                ]);
                $this->columnBuilder->add('district', Column::class, [
                    'title' => 'District'
                ]);
            }
            if ($this->authorizationChecker->isGranted('ROLE_QC')) {
                $this->columnBuilder->add('createdBy', Column::class, [
                    'title' => 'Created By'
                ]);
                $this->columnBuilder->add('district', Column::class, [
                    'title' => 'District'
                ]);
                $this->columnBuilder->add('createdAt', DateTimeColumn::class, [
                    'date_format' => 'YYYY-MM-DD',
                    'title' => 'Created Date'
                ]);
            }
            $this->columnBuilder->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'width' => '50px',
                'actions' => array(
                    array(
                        'route' => 'personnel_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Personnel';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'personnel_datatable';
    }
}
