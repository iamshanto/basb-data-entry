<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personnel
 *
 * @ORM\Table(name="personnels")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonnelRepository")
 */
class Personnel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var String
     * @ORM\Column(name="personnel_number", type="string", length=512, nullable=true)
     */
    private $personnelNumber;

    /**
     * @var String
     * @ORM\Column(name="designation", type="string", length=512, nullable=true)
     */
    private $designation;

    /**
     * @var String
     * @ORM\Column(name="name", type="string", length=512, nullable=true)
     */
    private $name;

    /**
     * @var String
     * @ORM\Column(name="service_type", type="string", length=512, nullable=true)
     */
    private $service;

    /**
     * @var String
     * @ORM\Column(name="regiment", type="string", length=512, nullable=true)
     */
    private $regiment;

    /**
     * @var String
     * @ORM\Column(name="trade", type="string", length=512, nullable=true)
     */
    private $trade;

    /**
     * @var String
     * @ORM\Column(name="fathers_name", type="string", length=512, nullable=true)
     */
    private $fathersName;

    /**
     * @var String
     * @ORM\Column(name="mothers_name", type="string", length=512, nullable=true)
     */
    private $mothersName;

    /**
     * @var String
     * @ORM\Column(name="inheritor", type="string", length=512, nullable=true)
     */
    private $inheritor;

    /**
     * @var String
     * @ORM\Column(name="wifes_name", type="string", length=512, nullable=true)
     */
    private $wifesName;

    /**
     * @var String
     * @ORM\Column(name="marriage_date", type="string", length=512, nullable=true)
     */
    private $marriageDate;

    /**
     * @var String
     * @ORM\Column(name="children1name", type="string", length=512, nullable=true)
     */
    private $children1Name;

    /**
     * @var String
     * @ORM\Column(name="children1dob", type="string", length=512, nullable=true)
     */
    private $children1Dob;

    /**
     * @var String
     * @ORM\Column(name="children1type", type="string", length=512, nullable=true)
     */
    private $children1Type;

    /**
     * @var String
     * @ORM\Column(name="children2name", type="string", length=512, nullable=true)
     */
    private $children2Name;

    /**
     * @var String
     * @ORM\Column(name="children2dob", type="string", length=512, nullable=true)
     */
    private $children2Dob;

    /**
     * @var String
     * @ORM\Column(name="children2type", type="string", length=512, nullable=true)
     */
    private $children2Type;

    /**
     * @var String
     * @ORM\Column(name="children3name", type="string", length=512, nullable=true)
     */
    private $children3Name;

    /**
     * @var String
     * @ORM\Column(name="children3dob", type="string", length=512, nullable=true)
     */
    private $children3Dob;

    /**
     * @var String
     * @ORM\Column(name="children3type", type="string", length=512, nullable=true)
     */
    private $children3Type;

    /**
     * @var String
     * @ORM\Column(name="children4name", type="string", length=512, nullable=true)
     */
    private $children4Name;

    /**
     * @var String
     * @ORM\Column(name="children4dob", type="string", length=512, nullable=true)
     */
    private $children4Dob;

    /**
     * @var String
     * @ORM\Column(name="children4type", type="string", length=512, nullable=true)
     */
    private $children4Type;

    /**
     * @var String
     * @ORM\Column(name="village", type="string", length=512, nullable=true)
     */
    private $village;

    /**
     * @var String
     * @ORM\Column(name="post_office", type="string", length=512, nullable=true)
     */
    private $postOffice;

    /**
     * @var String
     * @ORM\Column(name="upazilla", type="string", length=512, nullable=true)
     */
    private $upazilla;

    /**
     * @var String
     * @ORM\Column(name="district", type="string", length=512, nullable=true)
     */
    private $district;

    /**
     * @var String
     * @ORM\Column(name="mobile_number", type="string", length=512, nullable=true)
     */
    private $mobileNumber;

    /**
     * @var String
     * @ORM\Column(name="date_of_birth", type="string", length=512, nullable=true)
     */
    private $dateOfBirth;

    /**
     * @var String
     * @ORM\Column(name="joining_date", type="string", length=512, nullable=true)
     */
    private $joiningDate;

    /**
     * @var String
     * @ORM\Column(name="retirement_date", type="string", length=512, nullable=true)
     */
    private $retirementDate;

    /**
     * @var String
     * @ORM\Column(name="retirement_reason", type="string", length=512, nullable=true)
     */
    private $retirementReason;

    /**
     * @var String
     * @ORM\Column(name="last_served_place", type="string", length=512, nullable=true)
     */
    private $lastServedPlace;

    /**
     * @var String
     * @ORM\Column(name="last_served_unit", type="string", length=512, nullable=true)
     */
    private $lastServedUnit;

    /**
     * @var String
     * @ORM\Column(name="discipline_status", type="string", length=512, nullable=true)
     */
    private $disciplineStatus;

    /**
     * @var String
     * @ORM\Column(name="education", type="string", length=512, nullable=true)
     */
    private $education;

    /**
     * @var String
     * @ORM\Column(name="ts_number", type="string", length=512, nullable=true)
     */
    private $tsNumber;

    /**
     * @var String
     * @ORM\Column(name="nid", type="string", length=512, nullable=true)
     */
    private $nid;

    /**
     * @var String
     * @ORM\Column(name="reservist_last_date", type="string", length=512, nullable=true)
     */
    private $reservistLastDate;

    /**
     * @var String
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var String
     * @ORM\Column(name="created_by", type="string", length=255)
     */
    private $createdBy;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var String
     * @ORM\Column(name="update_by", type="string", length=255)
     */
    private $updateBy;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return String
     */
    public function getPersonnelNumber()
    {
        return $this->personnelNumber;
    }

    /**
     * @param String $personnelNumber
     *
     * @return Personnel
     */
    public function setPersonnelNumber($personnelNumber)
    {
        $this->personnelNumber = $personnelNumber;

        return $this;
    }

    /**
     * @return String
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param String $designation
     *
     * @return Personnel
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param String $name
     *
     * @return Personnel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return String
     */
    public function getRegiment()
    {
        return $this->regiment;
    }

    /**
     * @param String $regiment
     *
     * @return Personnel
     */
    public function setRegiment($regiment)
    {
        $this->regiment = $regiment;

        return $this;
    }

    /**
     * @return String
     */
    public function getTrade()
    {
        return $this->trade;
    }

    /**
     * @param String $trade
     *
     * @return Personnel
     */
    public function setTrade($trade)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * @return String
     */
    public function getFathersName()
    {
        return $this->fathersName;
    }

    /**
     * @param String $fathersName
     *
     * @return Personnel
     */
    public function setFathersName($fathersName)
    {
        $this->fathersName = $fathersName;

        return $this;
    }

    /**
     * @return String
     */
    public function getMothersName()
    {
        return $this->mothersName;
    }

    /**
     * @param String $mothersName
     *
     * @return Personnel
     */
    public function setMothersName($mothersName)
    {
        $this->mothersName = $mothersName;

        return $this;
    }

    /**
     * @return String
     */
    public function getInheritor()
    {
        return $this->inheritor;
    }

    /**
     * @param String $inheritor
     *
     * @return Personnel
     */
    public function setInheritor($inheritor)
    {
        $this->inheritor = $inheritor;

        return $this;
    }

    /**
     * @return String
     */
    public function getWifesName()
    {
        return $this->wifesName;
    }

    /**
     * @param String $wifesName
     *
     * @return Personnel
     */
    public function setWifesName($wifesName)
    {
        $this->wifesName = $wifesName;

        return $this;
    }

    /**
     * @return String
     */
    public function getMarriageDate()
    {
        return $this->marriageDate;
    }

    /**
     * @param String $marriageDate
     *
     * @return Personnel
     */
    public function setMarriageDate($marriageDate)
    {
        $this->marriageDate = $marriageDate;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren1Name()
    {
        return $this->children1Name;
    }

    /**
     * @param String $children1Name
     *
     * @return Personnel
     */
    public function setChildren1Name($children1Name)
    {
        $this->children1Name = $children1Name;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren1Dob()
    {
        return $this->children1Dob;
    }

    /**
     * @param String $children1Dob
     *
     * @return Personnel
     */
    public function setChildren1Dob($children1Dob)
    {
        $this->children1Dob = $children1Dob;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren1Type()
    {
        return $this->children1Type;
    }

    /**
     * @param String $children1Type
     *
     * @return Personnel
     */
    public function setChildren1Type($children1Type)
    {
        $this->children1Type = $children1Type;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren2Name()
    {
        return $this->children2Name;
    }

    /**
     * @param String $children2Name
     *
     * @return Personnel
     */
    public function setChildren2Name($children2Name)
    {
        $this->children2Name = $children2Name;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren2Dob()
    {
        return $this->children2Dob;
    }

    /**
     * @param String $children2Dob
     *
     * @return Personnel
     */
    public function setChildren2Dob($children2Dob)
    {
        $this->children2Dob = $children2Dob;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren2Type()
    {
        return $this->children2Type;
    }

    /**
     * @param String $children2Type
     *
     * @return Personnel
     */
    public function setChildren2Type($children2Type)
    {
        $this->children2Type = $children2Type;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren3Name()
    {
        return $this->children3Name;
    }

    /**
     * @param String $children3Name
     *
     * @return Personnel
     */
    public function setChildren3Name($children3Name)
    {
        $this->children3Name = $children3Name;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren3Dob()
    {
        return $this->children3Dob;
    }

    /**
     * @param String $children3Dob
     *
     * @return Personnel
     */
    public function setChildren3Dob($children3Dob)
    {
        $this->children3Dob = $children3Dob;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren3Type()
    {
        return $this->children3Type;
    }

    /**
     * @param String $children3Type
     *
     * @return Personnel
     */
    public function setChildren3Type($children3Type)
    {
        $this->children3Type = $children3Type;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren4Name()
    {
        return $this->children4Name;
    }

    /**
     * @param String $children4Name
     *
     * @return Personnel
     */
    public function setChildren4Name($children4Name)
    {
        $this->children4Name = $children4Name;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren4Dob()
    {
        return $this->children4Dob;
    }

    /**
     * @param String $children4Dob
     *
     * @return Personnel
     */
    public function setChildren4Dob($children4Dob)
    {
        $this->children4Dob = $children4Dob;

        return $this;
    }

    /**
     * @return String
     */
    public function getChildren4Type()
    {
        return $this->children4Type;
    }

    /**
     * @param String $children4Type
     *
     * @return Personnel
     */
    public function setChildren4Type($children4Type)
    {
        $this->children4Type = $children4Type;

        return $this;
    }

    /**
     * @return String
     */
    public function getVillage()
    {
        return $this->village;
    }

    /**
     * @param String $village
     *
     * @return Personnel
     */
    public function setVillage($village)
    {
        $this->village = $village;

        return $this;
    }

    /**
     * @return String
     */
    public function getPostOffice()
    {
        return $this->postOffice;
    }

    /**
     * @param String $postOffice
     *
     * @return Personnel
     */
    public function setPostOffice($postOffice)
    {
        $this->postOffice = $postOffice;

        return $this;
    }

    /**
     * @return String
     */
    public function getUpazilla()
    {
        return $this->upazilla;
    }

    /**
     * @param String $opazilla
     *
     * @return Personnel
     */
    public function setUpazilla($opazilla)
    {
        $this->upazilla = $opazilla;

        return $this;
    }

    /**
     * @return String
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param String $district
     *
     * @return Personnel
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * @return String
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * @param String $mobileNumber
     *
     * @return Personnel
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * @return String
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param String $dateOfBirth
     *
     * @return Personnel
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * @return String
     */
    public function getJoiningDate()
    {
        return $this->joiningDate;
    }

    /**
     * @param String $joiningDate
     *
     * @return Personnel
     */
    public function setJoiningDate($joiningDate)
    {
        $this->joiningDate = $joiningDate;

        return $this;
    }

    /**
     * @return String
     */
    public function getRetirementDate()
    {
        return $this->retirementDate;
    }

    /**
     * @param String $retirementDate
     *
     * @return Personnel
     */
    public function setRetirementDate($retirementDate)
    {
        $this->retirementDate = $retirementDate;

        return $this;
    }

    /**
     * @return String
     */
    public function getRetirementReason()
    {
        return $this->retirementReason;
    }

    /**
     * @param String $retirementReason
     *
     * @return Personnel
     */
    public function setRetirementReason($retirementReason)
    {
        $this->retirementReason = $retirementReason;

        return $this;
    }

    /**
     * @return String
     */
    public function getLastServedPlace()
    {
        return $this->lastServedPlace;
    }

    /**
     * @param String $lastServedPlace
     *
     * @return Personnel
     */
    public function setLastServedPlace($lastServedPlace)
    {
        $this->lastServedPlace = $lastServedPlace;

        return $this;
    }

    /**
     * @return String
     */
    public function getLastServedUnit()
    {
        return $this->lastServedUnit;
    }

    /**
     * @param String $lastServedUnit
     *
     * @return Personnel
     */
    public function setLastServedUnit($lastServedUnit)
    {
        $this->lastServedUnit = $lastServedUnit;

        return $this;
    }

    /**
     * @return String
     */
    public function getDisciplineStatus()
    {
        return $this->disciplineStatus;
    }

    /**
     * @param String $disciplineStatus
     *
     * @return Personnel
     */
    public function setDisciplineStatus($disciplineStatus)
    {
        $this->disciplineStatus = $disciplineStatus;

        return $this;
    }

    /**
     * @return String
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * @param String $education
     *
     * @return Personnel
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * @return String
     */
    public function getTsNumber()
    {
        return $this->tsNumber;
    }

    /**
     * @param String $tsNumber
     *
     * @return Personnel
     */
    public function setTsNumber($tsNumber)
    {
        $this->tsNumber = $tsNumber;

        return $this;
    }

    /**
     * @return String
     */
    public function getNid()
    {
        return $this->nid;
    }

    /**
     * @param String $nid
     *
     * @return Personnel
     */
    public function setNid($nid)
    {
        $this->nid = $nid;

        return $this;
    }

    /**
     * @return String
     */
    public function getReservistLastDate()
    {
        return $this->reservistLastDate;
    }

    /**
     * @param String $reservistLastDate
     *
     * @return Personnel
     */
    public function setReservistLastDate($reservistLastDate)
    {
        $this->reservistLastDate = $reservistLastDate;

        return $this;
    }

    /**
     * @return String
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param String $comments
     *
     * @return Personnel
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return String
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param String $createdBy
     *
     * @return Personnel
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return Personnel
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return String
     */
    public function getUpdateBy()
    {
        return $this->updateBy;
    }

    /**
     * @param String $updateBy
     *
     * @return Personnel
     */
    public function setUpdateBy($updateBy)
    {
        $this->updateBy = $updateBy;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updateAt
     *
     * @return Personnel
     */
    public function setUpdatedAt($updateAt)
    {
        $this->updatedAt = $updateAt;

        return $this;
    }

    /**
     * @return String
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @param String $office
     *
     * @return Personnel
     */
    public function setOffice($office)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * @return String
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param String $service
     *
     * @return Personnel
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }
}