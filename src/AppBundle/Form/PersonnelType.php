<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonnelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $childrenType = [
            'choices' => [
                'ছেলে' => 'ছেলে',
                'মেয়ে' => 'ছেলে'
            ]
        ];

        $builder
            ->add('personnelNumber', null, ['required' => true, 'label' => 'সৈনিং নং', 'attr' => ['autofocus' => true]])
            ->add('service', null, ['label' => 'বাহিনী'])
            ->add('designation', null, ['label' => 'পদবী'])
            ->add('name', null, ['label' => 'নাম'])
            ->add('regiment', null, ['label' => 'কোর/রেজিঃ'])
            ->add('trade', null, ['label' => 'ট্রেড/পেশা'])
            ->add('fathersName', null, ['label' => 'পিতার নাম'])
            ->add('mothersName', null, ['label' => 'মাতার নাম'])
            ->add('inheritor', null, ['label' => 'উত্তরাধীকারী'])// null, ['label' => 'উত্তরাধীকারী'])
            ->add('wifesName', null, ['label' => 'স্ত্রীর নাম'])
            ->add('marriageDate', null, ['label' => 'বিবাহের তারিখ', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('children1Name', null, ['label' => 'প্রথম সন্তানের নাম'])
            ->add('children1Dob', null, ['label' => 'প্রথম সন্তানের বয়স', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('children1Type', null, ['label' => ' '])
            //চতুর্থ সন্তানর ছেলে/মেয়ে
            ->add('children2Name', null, ['label' => 'দ্বিতীয় সন্তানের নাম'])
            ->add('children2Dob', null, ['label' => 'দ্বিতীয় সন্তানের বয়স', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('children2Type', null, ['label' => '  '])
            ->add('children3Name', null, ['label' => 'তৃতীয় সন্তানের নাম'])
            ->add('children3Dob', null, ['label' => 'তৃতীয় সন্তানের বয়স', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('children3Type', null, ['label' => '  ', ])
            ->add('children4Name', null, ['label' => 'চতুর্থ সন্তানের নাম'])
            ->add('children4Dob', null, ['label' => 'চতুর্থ সন্তানের বয়স', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('children4Type', null, ['label' => '  '])
            ->add('village', null, ['label' => 'গ্রাম'])
            ->add('postOffice', null, ['label' => 'পোস্ট অফিস'])
            ->add('upazilla', null, ['label' => 'থানা/উপজেলা'])
            ->add('district', null, ['label' => 'জেলা'])
            ->add('mobileNumber', null, ['label' => 'মোবাইল নম্বর'])
            ->add('dateOfBirth', null, ['label' => 'জন্ম তারিখ', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('joiningDate', null, ['label' => 'ভর্তির তারিখ', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('retirementDate', null, ['label' => 'অবসরের তারিখ', 'attr' => ['placeholder' => 'dd-mm-yy']])
            ->add('retirementReason', null, ['label' => 'অবসরের কারন'])
            ->add('lastServedPlace', null, ['label' => 'শেষ কর্মস্থল'])
            ->add('lastServedUnit', null, ['label' => 'শেষ ইউনিট'])
            ->add('disciplineStatus', null, ['label' => 'শৃংখলার মান'])
            ->add('education', null, ['label' => 'শিক্ষাগত যোগ্যতা'])
            ->add('tsNumber', null, ['label' => 'টিএস নম্বর'])
            ->add('nid', null, ['label' => 'জাতীয় পরিচয় নম্বর'])
            ->add('reservistLastDate', null, ['label' => 'রিজাভিষ্ট তারিখ'])
            ->add('comments', TextareaType::class, ['required' => false, 'label' => 'মন্তব্য']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Personnel'
        ));
    }

    public static function getDistricts()
    {
        $data = ["কক্সবাজার",
            "কিশোরগঞ্জ",
            "কুমিল্লা",
            "কুষ্টিয়া",
            "কুড়িগ্রাম",
            "খাগড়াছড়ি",
            "খুলনা",
            "গাইবান্ধা",
            "গাজীপুর",
            "গোপালগঞ্জ",
            "চট্টগ্রাম",
            "চাঁদপুর",
            "চাঁপাইনবাবগঞ্জ",
            "চুয়াডাঙ্গা",
            "জামালপুর",
            "জয়পুরহাট",
            "ঝালকাঠি",
            "ঝিনাইদহ",
            "টাঙ্গাইল",
            "ঠাকুরগাঁও",
            "ঢাকা",
            "দিনাজপুর",
            "নওগাঁ",
            "নরসিংদী",
            "নাটোর",
            "নারায়াণগঞ্জ",
            "নীলফামারী",
            "নেত্রকোণা",
            "নোয়াখালী",
            "নড়াইল",
            "পঞ্চগড়",
            "পটুয়াখালী",
            "পাবনা",
            "পিরোজপুর",
            "ফরিদপুর",
            "ফেনী",
            "বগুড়া",
            "বরগুনা",
            "বরিশাল",
            "বাগেরহাট",
            "বান্দরবান",
            "ব্রাহ্মণবাড়িয়া",
            "ভোলা",
            "মাগুরা",
            "মাদারীপুর",
            "মানিকগঞ্জ",
            "মুন্সিগঞ্জ",
            "মেহেরপুর",
            "মৌলভীবাজার",
            "ময়মনসিংহ",
            "যশোর",
            "রংপুর",
            "রাঙ্গামাটি",
            "রাজবাড়ী",
            "রাজশাহী",
            "লক্ষ্মীপুর",
            "লালমনিরহাট",
            "শরীয়তপুর",
            "শেরপুর",
            "সাতক্ষীরা",
            "সিরাজগঞ্জ",
            "সিলেট",
            "সুনামগঞ্জ",
            "হবিগঞ্জ"
        ];

        return array_combine($data, $data);
    }

    public static function getUpazilla()
    {
        $data = ["বাগেরহাট সদর",
            "চিতলমাড়ি",
            "ফকিরহাট",
            "কচুয়া",
            "মোল্লাহাট ",
            "মংলা",
            "মরেলগঞ্জ",
            "রামপাল",
            "স্মরণখোলা",
            "আলী কদম",
            "বান্দরবন সদর",
            "লামা",
            "নাইখংছড়ি ",
            "রউয়াংছড়ি ",
            "রুমা",
            "থানচি",
            "আমতলী",
            "বামনা",
            "বরগুনা সদর",
            "বেতাগি",
            "পাথরঘাটা",
            "তালতলী",
            "আগাইলঝরা",
            "বাবুগঞ্জ",
            "বাকেরগঞ্জ",
            "বানাড়িপারা",
            "বরিশাল সদর",
            "গৌরনদী",
            "হিজলা",
            "মেহেদিগঞ্জ ",
            "মুলাদি",
            "ওয়াজিরপুর",
            "ভোলা সদর",
            "বুরহানউদ্দিন",
            "চর ফ্যাশন",
            "দৌলতখান",
            "লালমোহন",
            "মনপুরা",
            "তাজুমুদ্দিন",
            "আদমদিঘী",
            "বগুড়া সদর",
            "ধুনট",
            "দুপচাচিয়া",
            "গাবতলি",
            "কাহালু",
            "নন্দিগ্রাম",
            "শাহজাহানপুর",
            "সারিয়াকান্দি",
            "শেরপুর",
            "শিবগঞ্জ",
            "সোনাতলা",
            "আখাউরা",
            "আশুগঞ্জ",
            "বাঞ্ছারামপুর",
            "বিজয় নগর",
            "ব্রাহ্মণবাড়িয়া সদর",
            "কসবা",
            "নবীনগর",
            "নাসির নগর",
            "সরাইল",
            "শাহবাজপুর টাউন",
            "চাঁদপুর সদর",
            "ফরিদগঞ্জ",
            "হাইমচর",
            "হাজীগঞ্জ",
            "কচুয়া",
            "মতলব দক্ষিণ",
            "মতলব উত্তর",
            "শাহরাস্তি",
            "আনোয়ারা",
            "বাশখালি",
            "বোয়ালখালি",
            "চন্দনাইশ",
            "ফটিকছড়ি",
            "হাঠহাজারী",
            "লোহাগারা",
            "মিরসরাই",
            "পটিয়া",
            "রাঙ্গুনিয়া",
            "রাউজান",
            "সন্দ্বীপ",
            "সাতকানিয়া",
            "সীতাকুণ্ড",
            "আলমডাঙ্গা",
            "চুয়াডাঙ্গা সদর",
            "দামুরহুদা",
            "জীবন নগর ",
            "বড়ুরা",
            "ব্রাহ্মণপাড়া",
            "বুড়িচং",
            "চান্দিনা",
            "চৌদ্দগ্রাম",
            "কুমিল্লা সদর",
            "কুমিল্লা সদর দক্ষিণ",
            "দাউদকান্দি",
            "দেবীদ্বার",
            "হোমনা",
            "লাকসাম",
            "মেঘনা",
            "মনোহরগঞ্জ",
            "মুরাদনগর",
            "নাঙ্গালকোট",
            "তিতাস",
            "চকরিয়া",
            "চকরিয়া",
            "কক্স বাজার সদর",
            "কুতুবদিয়া",
            "মহেশখালী",
            "পেকুয়া",
            "রামু",
            "টেকনাফ",
            "উখিয়া",
            "বিড়াল",
            "বিরামপুর",
            "বীরগঞ্জ",
            "বোচাগঞ্জ",
            "চিরিরবন্দর",
            "দিনাজপুর সদর",
            "ঘোড়াঘাট",
            "হাকিমপুর",
            "কাহারোল",
            "খানসামা",
            "নবাবগঞ্জ",
            "পার্বতীপুর",
            "ফুলবাড়ি",
            "আলফাডাঙ্গা",
            "ভাঙ্গা",
            "বোয়ালমারী",
            "চরভদ্রাসন ",
            "ফরিদপুর সদর",
            "মধুখালি",
            "নগরকান্ড",
            "সদরপুর",
            "শালথা",
            "ছাগল নাইয়া",
            "দাগানভিয়া",
            "ফেনী সদর",
            "ফুলগাজি",
            "পরশুরাম",
            "সোনাগাজি",
            "ফুলছড়ি",
            "গাইবান্ধা সদর",
            "গোবিন্দগঞ্জ",
            "পলাশবাড়ী",
            "সাদুল্যাপুর",
            "সাঘাটা",
            "সুন্দরগঞ্জ",
            "গাজীপুর সদর",
            "কালিয়াকৈর",
            "কালীগঞ্জ",
            "কাপাসিয়া",
            "শ্রীপুর",
            "টঙ্গি",
            "গোপালগঞ্জ সদর",
            "কাশিয়ানি",
            "কোটালিপাড়া",
            "মুকসুদপুর",
            "টুঙ্গিপাড়া",
            "আজমিরিগঞ্জ",
            "বাহুবল",
            "বানিয়াচং",
            "চুনারুঘাট",
            "হবিগঞ্জ সদর",
            "লাক্ষাই",
            "মাধবপুর",
            "নবীগঞ্জ",
            "শায়েস্তাগঞ্জ",
            "বকসিগঞ্জ",
            "দেওয়ানগঞ্জ",
            "ইসলামপুর",
            "জামালপুর সদর",
            "মাদারগঞ্জ",
            "মেলানদাহা",
            "নারুন্দি",
            "সরিষাবাড়ি ",
            "অভয়নগর",
            "বাঘের পাড়া ",
            "চৌগাছা",
            "যশোর সদর",
            "ঝিকরগাছা",
            "কেশবপুর",
            "মনিরামপুর ",
            "সারশা",
            "ঝালকাঠি সদর",
            "কাঁঠালিয়া",
            "নালচিতি",
            "রাজাপুর",
            "হাড়িনাকুন্দা",
            "ঝিনাইদহ সদর",
            "কালীগঞ্জ",
            "কোট চাঁদপুর ",
            "মহেশপুর",
            "শৈলকুপা",
            "আক্কেলপুর",
            "জয়পুরহাট সদর",
            "কালাই",
            "খেতলাল",
            "পাঁচবিবি",
            "দিঘিনালা ",
            "খাগড়াছড়ি",
            "লক্ষ্মীছড়ি",
            "মহলছড়ি",
            "মানিকছড়ি",
            "মাটিরাঙ্গা",
            "পানছড়ি",
            "রামগড়",
            "বাটিয়াঘাটা ",
            "ডাকপে",
            "দিঘলিয়া",
            "ডুমুরিয়া",
            "কয়ড়া",
            "পাইকগাছা",
            "ফুলতলা",
            "রূপসা",
            "তেরোখাদা",
            "অষ্টগ্রাম",
            "বাজিতপুর",
            "ভৈরব",
            "হোসেনপুর ",
            "ইটনা",
            "করিমগঞ্জ",
            "কতিয়াদি",
            "কিশোরগঞ্জ সদর",
            "কুলিয়ারচর",
            "মিঠামাইন",
            "নিকলি",
            "পাকুন্ডা",
            "তাড়াইল",
            "ভুরুঙ্গামারি",
            "চর রাজিবপুর",
            "চিলমারি",
            "কুড়িগ্রাম সদর",
            "নাগেশ্বরী",
            "ফুলবাড়ি",
            "রাজারহাট",
            "রউমারি",
            "উলিপুর",
            "ভেরামারা",
            "দৌলতপুর",
            "খোকসা",
            "কুমারখালি",
            "কুষ্টিয়া সদর",
            "মিরপুর",
            "কমল নগর",
            "লক্ষ্মীপুর সদর",
            "রায়পুর",
            "রামগঞ্জ",
            "রামগতি",
            "আদিতমারি",
            "হাতিবান্ধা",
            "কালীগঞ্জ",
            "লালমনিরহাট সদর",
            "পাটগ্রাম",
            "কালকিনি",
            "মাদারীপুর সদর",
            "রাজইর",
            "শিবচর",
            "মাগুরা সদর",
            "মোহাম্মাদপুর",
            "শালিখা",
            "শ্রীপুর",
            "দৌলতপুর",
            "ঘিওর",
            "হরিরামপুর",
            "মানিকগঞ্জ সদর",
            "সাঠুরিয়া",
            "শিবালয়",
            "সিঙ্গাইর",
            "বড়লেখা",
            "জুড়ি",
            "কামালগঞ্জ",
            "কুলাউরা",
            "মৌলভীবাজার",
            "রাজনগর",
            "শ্রীমঙ্গল",
            "আংনি",
            "মেহেরপুর সদর",
            "মুজিব নগর",
            "গজারিয়া",
            "লোহাজং",
            "মুন্সিগঞ্জ সদর",
            "সিরাজদিখান",
            "শ্রীনগর",
            "টঙ্গিবাড়ি",
            "ভালুকা",
            "ধবারুয়া",
            "ফুলবাড়িয়া",
            "গফরগাঁও",
            "গৌরিপুর",
            "হালুয়াঘাট",
            "ঈশ্বরগঞ্জ",
            "মুক্তাগাছা",
            "ময়মনসিং সদর",
            "নন্দাইল",
            "ফুলপুর",
            "ত্রিশাল",
            "আত্রাই",
            "বদলগাছি",
            "ধামইরহাট ",
            "মান্দা",
            "মহাদেবপুর",
            "নওগাঁ সদর",
            "নিয়ামতপুর",
            "পত্নীতলা",
            "পোরশা",
            "রাণীনগর",
            "সাপাহার",
            "কালিয়া",
            "লোহাগাড়া",
            "নড়াইল সদর",
            "আড়াইহাজার",
            "বান্দার",
            "নারায়ানগঞ্জ সদর",
            "রূপগঞ্জ",
            "সিদ্ধিরগঞ্জ",
            "সোনারগাঁও",
            "বেলাবো",
            "মনোহরদি",
            "নরসিংদী সদর",
            "পলাশ",
            "রায়পুর",
            "শিবপুর",
            "বাগাতিপাড়া",
            "বড়াইগ্রাম",
            "বড়াই গ্রাম",
            "লালপুর",
            "নাটোর সদর",
            "নাটোর সদর",
            "ভোলাহাট",
            "গোমস্তাপুর",
            "নাচোল",
            "নবাবগঞ্জ সদর",
            "শিবগঞ্জ",
            "আটপাড়া",
            "বরহাট্টা",
            "দুর্গাপুর",
            "কলমাকান্দা",
            "কেন্দুয়া",
            "খালিয়াজুরি",
            "মদন",
            "মোহনগঞ্জ",
            "নেত্রকোনা সদর",
            "পূর্বধলা",
            "ডিমলা",
            "ডোমার",
            "জলঢাকা",
            "কিশোরগঞ্জ",
            "নীলফামারী সদর",
            "সৈয়দপুর",
            "বেগমগঞ্জ",
            "চাটখিল",
            "কোম্পানীগঞ্জ",
            "হাতিয়া",
            "কবিরহাট ",
            "নোয়াখালী সদর",
            "শেনবাগ",
            "সোনাইমুরি",
            "সুবর্ণ চর ",
            "আটঘরিয়া",
            "বেড়া",
            "ভাঙ্গুরা",
            "চাটমোহর",
            "ফরিদপুর",
            "ঈশ্বরদী",
            "পাবনা সদর",
            "সাথিয়া",
            "সুজানগর",
            "আটোয়ারি",
            "বোদা",
            "দেবীগঞ্জ",
            "পঞ্চগড় সদর",
            "তেতুলিয়া",
            "বাউফল",
            "দশমিনা",
            "ডুমকি",
            "গলাচিপা",
            "কালাপারা",
            "মির্জাগঞ্জ ",
            "পটুয়াখালী সদর",
            "রাঙ্গাবালি",
            "ভ্যান্ডারিয়া",
            "কাউখালি",
            "মাঠবাড়িয়া",
            "নাজিরপুর",
            "নেসারাবাদ",
            "পিরোজপুর সদর",
            "জিয়ানগর",
            "বালিয়াকান্দি",
            "গোয়ালন্দ ঘাট",
            "কালুখালি",
            "পাংশা",
            "রাজবাড়ি সদর",
            "বাঘা",
            "বাগমারা",
            "চারঘাট",
            "দুর্গাপুর",
            "গোদাগারি",
            "মোহনপুর",
            "পবা",
            "পুঠিয়া",
            "তানোর",
            "বাঘাইছড়ি",
            "বরকল",
            "বেলাইছড়ি",
            "জুরাইছড়ি",
            "কাপ্তাই",
            "কাউখালি",
            "লাঙ্গাডু",
            "নান্নেরচর ",
            "রাজাস্থলি",
            "রাঙ্গামাটি সদর",
            "বদরগঞ্জ",
            "গঙ্গাচরা",
            "কাউনিয়া",
            "মিঠাপুকুর",
            "পীরগাছা",
            "পীরগঞ্জ",
            "রংপুর সদর",
            "তারাগঞ্জ",
            "আসসাশুনি ",
            "দেভাটা",
            "কলরোয়া",
            "কালীগঞ্জ",
            "সাতক্ষীরা সদর",
            "শ্যামনগর",
            "তালা",
            "ভেদারগঞ্জ",
            "দামুদিয়া",
            "গোসাইর হাট ",
            "জাজিরা",
            "নড়িয়া",
            "শরীয়তপুর সদর ",
            "ঝিনাইগাতি",
            "নাকলা",
            "নালিতাবাড়ি",
            "শেরপুর সদর",
            "শ্রীবরদি",
            "বেলকুচি",
            "চৌহালি",
            "কামারখান্দা",
            "কাজীপুর",
            "রায়গঞ্জ",
            "শাহজাদপুর",
            "সিরাজগঞ্জ সদর",
            "তারাশ",
            "উল্লাপাড়া",
            "বিসশম্ভারপুর",
            "ছাতক",
            "দেড়াই",
            "ধরমপাশা",
            "দোয়ারাবাজার",
            "জগন্নাথপুর",
            "জামালগঞ্জ",
            "শান্তিগঞ্জ",
            "সুল্লা",
            "সুনামগঞ্জ সদর",
            "তাহিরপুর",
            "বালাগঞ্জ",
            "বেয়ানিবাজার",
            "বিশ্বনাথ",
            "কোম্পানিগঞ্জ",
            "দক্ষিণ সুরমা",
            "ফেঞ্চুগঞ্জ",
            "গোলাপগঞ্জ",
            "গোয়াইনঘাট",
            "জয়ন্তপুর",
            "কানাইঘাট",
            "নবীগঞ্জ",
            "সিলেট সদর",
            "জাকিগঞ্জ",
            "বসাইল",
            "ভুয়াপুর",
            "দেলদুয়ার",
            "ধানবাড়ি",
            "ঘাটাইল",
            "গোপালপুর",
            "কালিহাতি",
            "মধুপুর",
            "মির্জাপুর",
            "নগরপুর",
            "সখিপুর",
            "টাঙ্গাইল সদর",
            "বালিয়াডাঙ্গি",
            "হরিপুর",
            "পীরগঞ্জ",
            "রাণীসংকইল",
            "ঠাকুরগাঁও সদর",
        ];

        return array_combine($data, $data);
    }

    public function getDisciplineStatus()
    {
        $data = [
            'আদর্শ',
            'ভাল',
            'সন্তোষজনক',
            'অসন্তোষজনক'
        ];
        return array_combine($data, $data);
    }
}
